// // Soal No. 1 (Anonymous Function)
// int kalikanMath(int aa, int bb, Function ops) {
//   return ops(aa, bb);
// }

// void main() {
//   print(kalikanMath(3, 2, (a, b) => a * b));
// }

// // Soal No. 2 (Lambda Expression)
// int kalikan(int aa, int bb) => aa * bb;
// void main() {
//   print(kalikan(3, 4));
// }

// // Soal No. 3 (Positional Parameters)
// String say(String to, String message, [String? from]) {
//   return message + " " + to + ", " + ((from != null) ? "from " + from : "");
// }

// void main(List<String> args) {
//   print(say("Abduh", "Hello", "Muhammad"));
// }

// // Soal No. 4 (Function Async)
// void main(List<String> args) async {
//   print("Seluruh kota merupakan tempat bermain yang asyik");
//   await line();
//   await line2();
//   await line3();
//   await line4();
//   await line5();
//   await line6();
//   await line7();
//   await line8();
//   await line9();
// }

// Future<void> line() async {
//   return await Future.delayed(
//       Duration(seconds: 8), () => print("Oh senangnya aku senang sekali"));
// }

// Future<void> line2() async {
//   return await Future.delayed(
//       Duration(seconds: 7), () => print("Kalau begini akupun jadi sibuk"));
// }

// Future<void> line3() async {
//   return await Future.delayed(
//       Duration(seconds: 7), () => print("Berusaha mengejar-ngejar dia"));
// }

// Future<void> line4() async {
//   return await Future.delayed(Duration(seconds: 7),
//       () => print("Matahari menyinari semua perasaan cinta"));
// }

// Future<void> line5() async {
//   return await Future.delayed(Duration(seconds: 9),
//       () => print("Tapi mengapa hanya aku yang dimarahi"));
// }

// Future<void> line6() async {
//   return await Future.delayed(Duration(seconds: 8),
//       () => print("Di musim panas merupakan hari bermain gembira"));
// }

// Future<void> line7() async {
//   return await Future.delayed(Duration(seconds: 8),
//       () => print("Sang gajah terkena flu pilek tiada henti-hentinya"));
// }

// Future<void> line8() async {
//   return await Future.delayed(Duration(seconds: 10),
//       () => print("Sang beruang tidur dan tak ada yang berani ganggu dia"));
// }

// Future<void> line9() async {
//   return await Future.delayed(
//       Duration(seconds: 9), () => print("Oh sibuknya aku sibuk sekali"));
// }
