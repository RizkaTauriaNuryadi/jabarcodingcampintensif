// Soal 3 (Polymorism)
// import 'jenis_pemrograman.dart'; //memanggil object jenis_pemrograman.dart
// import 'pemrograman_java.dart'; //memanggil class pemrograman java
// import 'pemrograman_php.dart'; //memanggil class pemrograman php
// import 'pemrograman_javascript.dart'; //memanggil class pemrograman javascript
// import 'pemrograman_python.dart'; //memanggil class pemrograman phyton

// void main(List<String> args) {
//   JenisPemrograman jenisPemrograman =
//       new JenisPemrograman(); //menginisialisasi jenisPemrograman sebagai JenisPemrograman
//   PemrogramanJava pemrogramanJava =
//       new PemrogramanJava(); //menginisialisasi pemrogramanJava sebagai PemrogramanJava
//   PemrogramanPHP pemrogramanPHP =
//       new PemrogramanPHP(); //menginisialisasi pemrogramanPHP sebagai PemrogramanPHP
//   PemrogramanJavascript pemrogramanJavascript =
//       new PemrogramanJavascript(); //menginisialisasi pemrogramanJavascript sebagai PemrogramanJavascript
//   PemrogramanPhyton pemrogramanPhyton =
//       new PemrogramanPhyton(); //menginisialisasi pemrogramanPhyton sebagai PemrogramanPhyton

//   jenisPemrograman.kegunaan();
//   jenisPemrograman.tingkatPenguasaan();

//   print("Kegunaan Pemrograman Java: ${pemrogramanJava.kegunaan()}");
//   print(
//       "Tingkat Penguasaan Pemrograman Java: ${pemrogramanJava.tingkatPenguasaan()}");
//   print("Kegunaan Pemrograman PHP: ${pemrogramanPHP.kegunaan()}");
//   print(
//       "Tingkat Penguasaan Pemrograman PHP: ${pemrogramanPHP.tingkatPenguasaan()}");
//   print("Kegunaan Pemrograman Javascript: ${pemrogramanJavascript.kegunaan()}");
//   print(
//       "Tingkat Penguasaan Pemrograman Javascript: ${pemrogramanJavascript.tingkatPenguasaan()}");
//   print("Kegunaan Pemrograman Phyton: ${pemrogramanPhyton.kegunaan()}");
//   print(
//       "Tingkat Penguasaan Pemrograman Phyton: ${pemrogramanPhyton.tingkatPenguasaan()}");
// }
