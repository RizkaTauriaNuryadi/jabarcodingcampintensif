// Soal No.1 (Enkapsulasi)
// class SegitigaSamaKaki {
//   late double _a; //initialisasi tipe data alas // late sebagai null safety
//   late double _t; //initialisasi tipe data tinggi // late sebagai null safety
//   void setA(double value) {
//     if (value < 0) {
//       //validasi jika nilai -(min)
//       value = value * -1; //akan dikalikan -1 agar nilai tetap positif
//     }
//     _a = value; //alias
//   }

//   void setT(double value) {
//     if (value < 0) {
//       //validasi jika nilai -(min)
//       value = value * -1; //akan dikalikan -1 agar nilai tetap positif
//     }
//     _t = value; //alias
//   }

//   double getA() {
//     //get a
//     return _a; //mengembalikan nilai get jari2
//   }

//   double getT() {
//     //get t
//     return _t; //mengembalikan nilai get jari2
//   }

//   double get luas => 0.5 * _a * _t; //menghitung luas
// }
