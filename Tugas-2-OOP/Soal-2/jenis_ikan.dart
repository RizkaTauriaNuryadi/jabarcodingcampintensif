// Soal 2 (Inheritance)
// //pembuatan class jenis_ikan yang memiliki _tenagaIkan; yang akan men-get dan men-set nilai dari tenagaIkan masing masing turunannya
// class JenisIkan {
//   late int
//       _tenagaIkan; //inisialisasi tipe data tenagaIkan //late sebagai null safety

//   int get tenagaIkan => _tenagaIkan; //get nilai tenagaIkan
//   set tenagaIkan(int value) {
//     //set nilai tenagaIkan
//     _tenagaIkan = value;
//   }
// }
