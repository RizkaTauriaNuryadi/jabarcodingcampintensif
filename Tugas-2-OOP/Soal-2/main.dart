// Soal 2 (Inheritance)
// import 'ikan_tongkol.dart';
// import 'ikan_layur.dart';
// import 'ikan_pari.dart';
// import 'ikan_hiu.dart';

// void main(List<String> args) {
//   IkanTongkol ikanTongkol =
//       IkanTongkol(); //inisialisasi ikanTongkol sebagai ikan tongkol
//   IkanLayur ikanLayur = IkanLayur(); //inisialisasi ikanLayur sebagai ikan layur
//   IkanPari ikanPari = IkanPari(); //inisialisasi ikanPari sebagai ikan pari
//   IkanHiu ikanHiu = IkanHiu(); //inisialisasi ikanHiu sebagai ikanHiu

//   ikanTongkol.tenagaIkan = 4; //menambahkan value tenagaIkan ikan tongkol
//   ikanLayur.tenagaIkan = 5; //menambahkan value tenagaIkan ikan layur
//   ikanPari.tenagaIkan = 8; //menambahkan value tenagaIkan ikan pari
//   ikanHiu.tenagaIkan = 9; //menambahkan value tenagaIkan ikan hiu

//   //menampilkan tenagaIkan dan conditional jika tenagaIkan di bawah 5 maka akan di cetak 5
//   if (ikanTongkol.tenagaIkan <= 5) {
//     print("tenaga ikan ikan tongkol: 5");
//   } else {
//     print("tenaga ikan ikan tongkol: ${ikanTongkol.tenagaIkan}");
//   }
//   if (ikanLayur.tenagaIkan <= 5) {
//     print("tenaga ikan ikan layur: 5");
//   } else {
//     print("tenaga ikan ikan layur: ${ikanLayur.tenagaIkan}");
//   }
//   if (ikanPari.tenagaIkan <= 5) {
//     print("tenaga ikan ikan pari: 5");
//   } else {
//     print("tenaga ikan ikan pari: ${ikanPari.tenagaIkan}");
//   }
//   if (ikanHiu.tenagaIkan <= 5) {
//     print("tenaga ikan ikan hiu: 5");
//   } else {
//     print("tenaga ikan ikan hiu: ${ikanHiu.tenagaIkan}");
//   }

//   //menampilkan masing masing object yang ada pada class masing-masing yang terdapat pada child class
//   print("object dari ikan tongkol: " + ikanTongkol.bergerombol());
//   print("object dari ikan layur: " + ikanLayur.sendiri());
//   print("object dari ikan pari: " + ikanPari.hap());
//   print("object dari ikan hiu: " + ikanHiu.wush());
// }
