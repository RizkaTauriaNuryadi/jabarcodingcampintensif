// Soal 1 (Enkapsulasi)
// import 'segitiga_sama_kaki.dart';

// void main(List<String> args) {
//   SegitigaSamaKaki segitigaSamaKaki; //inisialisasi segitiga sama kaki
//   double luasSegitigaSamaKaki; //inisialisasi tipe data luas segitiga sama kaki
//   segitigaSamaKaki =
//       new SegitigaSamaKaki(); //menginisialisasi atau mengaliskan  segitigaSamaKaki sebagai segitiga sama kaki/ pointer menunjuk object segitiga sama kaki
//   segitigaSamaKaki.setA(4.0); //set nilai alas
//   segitigaSamaKaki.setT(8.0); //set nilai tinggi

//   luasSegitigaSamaKaki = segitigaSamaKaki.luas; //alias luas segitiga sama kaki
//   print(luasSegitigaSamaKaki); //mencetak luas segitiga sama kaki
// }
